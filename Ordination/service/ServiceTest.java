package service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;

public class ServiceTest {

	private Service s = Service.getTestService();
	private Patient patient = s.opretPatient("Test Testesen", "010180-0101", 0);
	private Laegemiddel laegemiddel = s.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

	/*
	 * Der testes på metoderne opretDagligFastOrdination() og
	 * anbefaletDosisPrDoegn()
	 */

	// Tester om der kastes IllegalArgumentException når startdatoen er efter slutdatoen
	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination_TC1() {
		s.opretDagligFastOrdination(LocalDate.of(2016, 9, 19), LocalDate.of(2016, 9, 18), patient, laegemiddel, 1, 2, 3,
				4);
	}

	// Tester om DagligFast objekt oprettes når startdatoen er lig med slutdatoen
	@Test
	public void testOpretDagligFastOrdination_TC2() {
		DagligFast df = s.opretDagligFastOrdination(LocalDate.of(2016, 9, 19), LocalDate.of(2016, 9, 19), patient,
				laegemiddel, 1, 2, 3, 4);
		assertEquals(patient.getOrdinationer().get(0), df);
	}

	// Tester om DagligFast objekt oprettes når startdatoen er efter slutdatoen
	@Test
	public void testOpretDagligFastOrdination_TC3() {
		DagligFast df = s.opretDagligFastOrdination(LocalDate.of(2016, 9, 19), LocalDate.of(2016, 9, 20), patient,
				laegemiddel, 1, 2, 3, 4);
		assertEquals(patient.getOrdinationer().get(0), df);
	}

	// Tester om metoden returnerer null når antal enheder sættes til -1
	@Test
	public void testOpretDagligFastOrdination_TC4() {
		DagligFast df = s.opretDagligFastOrdination(LocalDate.of(2016, 9, 19), LocalDate.of(2016, 9, 20), patient,
				laegemiddel, -1, -1, -1, -1);
		assertEquals(null, df);
	}

	// Tester om DagligFast objekt oprettes når antal enheder sættes til 0
	@Test
	public void testOpretDagligFastOrdination_TC5() {
		DagligFast df = s.opretDagligFastOrdination(LocalDate.of(2016, 9, 19), LocalDate.of(2016, 9, 20), patient,
				laegemiddel, 0, 0, 0, 0);
		assertEquals(patient.getOrdinationer().get(0), df);
	}

	// Tester om DagligFast objekt oprettes når antal enheder sættes til Double.MAX_VALUE
	@Test
	public void testOpretDagligFastOrdination_TC6() {
		DagligFast df = s.opretDagligFastOrdination(LocalDate.of(2016, 9, 19), LocalDate.of(2016, 9, 20), patient,
				laegemiddel, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
		assertEquals(patient.getOrdinationer().get(0), df);
	}

	// Tester om outputtet er korrekt for en patient med en vægt på 0 kg (Let)
	@Test
	public void testAnbefaletDosisPrDoegn_TC1() {
		assertEquals(0, Service.getTestService().anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
	}

	// Tester om outputtet er korrekt for en patient med en vægt på 24.9 kg (Let)
	@Test
	public void testAnbefaletDosisPrDoegn_TC2() {
		patient.setVaegt(24.9);
		assertEquals(24.9, Service.getTestService().anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
	}

	// Tester om outputtet er korrekt for en patient med en vægt på 25 kg (Normal)
	@Test
	public void testAnbefaletDosisPrDoegn_TC3() {
		patient.setVaegt(25);
		assertEquals(37.5, Service.getTestService().anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
	}

	// Tester om outputtet er korrekt for en patient med en vægt på 119.9 kg (Normal)
	@Test
	public void testAnbefaletDosisPrDoegn_TC4() {
		patient.setVaegt(119.9);
		assertEquals(179.85, Service.getTestService().anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
	}

	// Tester om outputtet er korrekt for en patient med en vægt på 120 kg (Tung)
	@Test
	public void testAnbefaletDosisPrDoegn_TC5() {
		patient.setVaegt(120);
		assertEquals(180, Service.getTestService().anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
	}

	// Tester om outputtet er korrekt for en patient med en vægt på 125 kg (Tung)
	@Test
	public void testAnbefaletDosisPrDoegn_TC6() {
		patient.setVaegt(125);
		assertEquals(250, Service.getTestService().anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
	}
}
