package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Test;

public class DagligSkaevTest {

	private LocalTime[] klokkeSlet = new LocalTime[] { LocalTime.of(8, 00), LocalTime.of(12, 00), LocalTime.of(18, 00),
			LocalTime.of(00, 00) };
	private double[] enheder = new double[] { 2, 2, 3, 3 };
	private DagligSkaev ds = new DagligSkaev(LocalDate.of(2016, 9, 20), LocalDate.of(2016, 9, 21), klokkeSlet, enheder);

	// Tester metoden doegnDosis(). Ordination anvendt 4 gange med henholdsvis 2, 2, 3 og 3 enheder i doserne
	@Test
	public void testDoegnDosis() {
		assertEquals(10, ds.doegnDosis(), 0.001);
	}

	// Tester metoden samletDosis(). Ordination anvendt 4 gange med henholdsvis 2, 2, 3 og 3 enheder i doserne og ordinationsperiode på 2 dage 
	@Test
	public void testSamletDosis() {
		assertEquals(20, ds.samletDosis(), 0.001);
	}

	// Tester om metoden getType() henter den korrekte type ordination
	@Test
	public void testGetType() {
		assertEquals("Daglig Skaev", ds.getType());
	}

	// Tester metoden getDoser(). Tester om ArrayListen med doser returneres
	@Test
	public void testGetDoser() {
		ArrayList<Dosis> doser = new ArrayList<Dosis>();
		doser.addAll(ds.getDoser());
		assertEquals(doser, ds.getDoser());
	}

	// Tester metoden opretDosis(). Tester om der tilføjes en dosis til ArrayListen af doser ved oprettelse af dosis
	@Test
	public void testOpretDosis() {
		assertEquals(4, ds.getDoser().size());
		ds.opretDosis(LocalTime.of(10, 00), 2);
		assertEquals(5, ds.getDoser().size());
	}

}
