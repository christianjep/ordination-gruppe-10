package ordination;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class DagligFastTest {

	private DagligFast df = new DagligFast(LocalDate.of(2016, 9, 20), LocalDate.of(2016, 9, 21), 2, 2, 3, 3);

	// Tester metoden doegnDosis(). Ordination anvendt 4 gange med henholdsvis 2, 2, 3 og 3 enheder i doserne
	@Test
	public void testDoegnDosis() {
		assertEquals(10, df.doegnDosis(), 0.001);
	}

	// Tester metoden samletDosis(). Ordination anvendt 4 gange med henholdsvis 2, 2, 3 og 3 enheder i doserne og ordinationsperiode på 2 dage
	@Test
	public void testSamletDosis() {
		assertEquals(20, df.samletDosis(), 0.001);
	}

	// Tester om metoden getType() henter den korrekte type ordination
	@Test
	public void testGetType() {
		assertEquals("Daglig Fast", df.getType());
	}

	// Tester metoden getDoser(). Tester om Arrayet med doser returneres
	@Test
	public void testGetDoser() {
		Dosis[] doser = df.getDoser().clone();
		assertArrayEquals(doser, df.getDoser());
	}
}
