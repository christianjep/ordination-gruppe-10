package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, double morgenAntal, double middagAntal, double aftenAntal,
			double natAntal) {
		super(startDen, slutDen);
		Dosis morgen = new Dosis(LocalTime.of(8, 00), morgenAntal);
		Dosis middag = new Dosis(LocalTime.of(12, 00), middagAntal);
		Dosis aften = new Dosis(LocalTime.of(18, 00), aftenAntal);
		Dosis nat = new Dosis(LocalTime.of(00, 00), natAntal);
		doser[0] = morgen;
		doser[1] = middag;
		doser[2] = aften;
		doser[3] = nat;
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis d : doser) {
			sum = sum + d.getAntal();
		}
		return sum;
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}

	public Dosis[] getDoser() {
		return doser;
	}
}
