package ordination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;

public class PNTest {

	private PN pn = new PN(LocalDate.of(2016, 9, 20), LocalDate.of(2016, 9, 21), 10);

	// Tester metoden givDosis(). Tester om metoden returnerer true ved dato indenfor ordinationsperiode
	@Test
	public void testGivDosisIndenforPeriode() {
		assertTrue(pn.givDosis(LocalDate.of(2016, 9, 20)));
	}

	// Tester metoden givDosis(). Tester om metoden returnerer false ved dato udenfor ordinationsperiode
	@Test
	public void testGivDosisUdenforPeriode() {
		assertFalse(pn.givDosis(LocalDate.of(2016, 9, 22)));
	}

	// Tester metoden doegnDosis(). Ordination anvendt 2 gange med 4 enheder pr. dosis og 2 dage mellem foerste og sidste anvendelse
	@Test
	public void testDoegnDosisAnvendt2Gange() {
		int antalGangeAnvendt = 2;
		int antalEnheder = 4;
		int antalDageMellemFoerstogSidst = 2;
		double forventetOutput = (antalGangeAnvendt * antalEnheder) / antalDageMellemFoerstogSidst;
		pn.givDosis(LocalDate.of(2016, 9, 20));
		pn.givDosis(LocalDate.of(2016, 9, 21));
		pn.setAntalEnheder(antalEnheder);
		assertEquals(forventetOutput, pn.doegnDosis(), 0.0001);
	}

	// Tester metoden doegnDosis(). Ordination anvendt 0 gange med 4 enheder pr. dosis
	@Test
	public void testDoegnDosisAnvendt0Gange() {
		int antalEnheder = 4;
		pn.setAntalEnheder(antalEnheder);
		assertEquals(0, pn.doegnDosis(), 0.0001);

	}

	// Tester metoden samletDosis(). Ordination anvendt 2 gange med 4 enheder pr. dosis
	@Test
	public void testSamletDosisAnvendt2Gange() {
		int antalGangeAnvendt = 2;
		int antalEnheder = 4;
		int forventetOutput = antalGangeAnvendt * antalEnheder;
		pn.setAntalEnheder(antalEnheder);
		pn.givDosis(LocalDate.of(2016, 9, 20));
		pn.givDosis(LocalDate.of(2016, 9, 21));
		assertEquals(forventetOutput, pn.samletDosis(), 0.0001);
	}

	// Tester metoden getAntalGangeGivet(). Ordination anvendt 2 gange
	@Test
	public void testAntalGangeGivet2GangeGivet() {
		int antalGangeGivet = 2;
		pn.givDosis(LocalDate.of(2016, 9, 20));
		pn.givDosis(LocalDate.of(2016, 9, 21));
		assertEquals(antalGangeGivet, pn.getAntalGangeGivet());
	}

	// Tester metoden getAntalEnheder(). Tester om antal enheder returneres
	@Test
	public void testGetAntalEnheder() {
		double antalEnheder = pn.getAntalEnheder();
		assertEquals(antalEnheder, pn.getAntalEnheder(), 0.001);
	}

	// Tester metoden setAntalEnheder(). Tester om antal enheder sættes
	@Test
	public void testSetAntalEnheder() {
		assertEquals(10, pn.getAntalEnheder(), 0.001);
		pn.setAntalEnheder(20);
		assertEquals(20, pn.getAntalEnheder(), 0.001);
	}

	// Tester om metoden getType() henter den korrekte type ordination
	@Test
	public void testGetType() {
		assertEquals("PN", pn.getType());
	}

	// Tester metoden getDoser(). Tester om ArrayList<LocalDate> med dosedatoer returneres
	@Test
	public void testGetDoser() {
		ArrayList<LocalDate> doser = new ArrayList<LocalDate>();
		doser.addAll(pn.getDoser());
		assertEquals(doser, pn.getDoser());
	}

	// Tester metoden addDosis(). Tester om der tilføjes en dosis til ArrayList<LocalDate> med dosedatoer
	@Test
	public void testAddDosis() {
		assertEquals(0, pn.getDoser().size());
		pn.addDosis(LocalDate.of(2016, 9, 22));
		assertEquals(1, pn.getDoser().size());
	}

	// Tester metoden removeDosis(). Tester om der fjernes en dosis fra ArrayList<LocalDate> med dosedatoer
	@Test
	public void testRemoveDosis() {
		pn.addDosis(LocalDate.of(2016, 9, 22));
		assertEquals(1, pn.getDoser().size());
		pn.removeDosis(LocalDate.of(2016, 9, 22));
		assertEquals(0, pn.getDoser().size());
	}
}
