package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, LocalTime[] klokkeSlet, double[] antalEnheder) {
		super(startDen, slutDen);
		for (int i = 0; i < klokkeSlet.length; i++) {
			opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis d : doser) {
			sum = sum + d.getAntal();
		}
		return sum;
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public String getType() {
		return "Daglig Skaev";
	}

	public ArrayList<Dosis> getDoser() {
		return doser;
	}

	public void opretDosis(LocalTime tid, double antal) {
		doser.add(new Dosis(tid, antal));
	}
}
