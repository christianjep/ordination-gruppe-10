package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {
	private double antalEnheder;
	private ArrayList<LocalDate> doser = new ArrayList<LocalDate>();
	private LocalDate førsteDag;
	private LocalDate sidsteDag;

	public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
		super(startDen, slutDen);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isAfter(getStartDen().minusDays(1)) && givesDen.isBefore(getSlutDen().plusDays(1))) {
			if (førsteDag == null) {
				førsteDag = givesDen;
				sidsteDag = givesDen;
			}
			if (sidsteDag.isBefore(givesDen)) {
				sidsteDag = givesDen;
			}
			if (givesDen.isBefore(førsteDag)) {
				førsteDag = givesDen;
			}
			doser.add(givesDen);
			return true;
		}
		return false;
	}

	@Override
	public double doegnDosis() {
		if (this.samletDosis() <= 0) {
			return 0;
		} else if ((int) ChronoUnit.DAYS.between(førsteDag, sidsteDag) == 0) {
			return this.samletDosis();
		} else {
			return this.samletDosis() / ((int) ChronoUnit.DAYS.between(førsteDag, sidsteDag) + 1);
		}
	}

	@Override
	public double samletDosis() {
		return doser.size() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return doser.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public void setAntalEnheder(double antalEnheder) {
		this.antalEnheder = antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

	public ArrayList<LocalDate> getDoser() {
		return doser;
	}

	public void addDosis(LocalDate dosisDato) {
		doser.add(dosisDato);
	}

	public void removeDosis(LocalDate dosisDato) {
		doser.remove(dosisDato);
	}
}
