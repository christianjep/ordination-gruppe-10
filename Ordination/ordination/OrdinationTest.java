package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class OrdinationTest {

	private LocalTime[] klokkeSlet = new LocalTime[] { LocalTime.of(8, 00), LocalTime.of(12, 00), LocalTime.of(18, 00),
			LocalTime.of(00, 00) };
	private double[] enheder = new double[] { 2, 2, 3, 3 };
	private Ordination ordination = new DagligSkaev(LocalDate.of(2016, 9, 20), LocalDate.of(2016, 9, 21), klokkeSlet,
			enheder);
	private Laegemiddel lm1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
	private Laegemiddel lm2 = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

	@Before
	public void setUp() throws Exception {
		ordination.setLaegeMiddel(lm1);
	}

	/*
	 * Abstrakte metoder i Ordination er undladt, da disse testes i henholdsvis
	 * DagligFastTest, DagligSkaevTest og PNTest
	 */

	// Tester metoden getStartDen(). Tester om startdato returneres
	@Test
	public void testGetStartDen() {
		assertEquals(LocalDate.of(2016, 9, 20), ordination.getStartDen());
	}

	// Tester metoden getSlutDen(). Tester om slutdato returneres
	@Test
	public void testGetSlutDen() {
		assertEquals(LocalDate.of(2016, 9, 21), ordination.getSlutDen());
	}

	// Tester metoden antalDage(). Tester antal dage mellem start og slutdato. 2 dage mellem start og slut.
	@Test
	public void testAntalDage() {
		assertEquals(2, ordination.antalDage());
	}

	// Tester metoden toString(). Tester om startdato returneres som String
	@Test
	public void testGetToString() {
		assertEquals(ordination.getStartDen().toString(), ordination.toString());
	}

	// Tester metoden getLaegemiddel(). Tester om laegeMiddel returneres
	@Test
	public void testGetLaegemiddel() {
		assertEquals(lm1, ordination.getLaegemiddel());
	}

	// Tester metoden setLaegemiddel(). Tester om laegeMiddel sættes
	@Test
	public void testSetLaegemiddel() {
		assertEquals(lm1, ordination.getLaegemiddel());
		ordination.setLaegeMiddel(lm2);
		assertEquals(lm2, ordination.getLaegemiddel());
	}
}
